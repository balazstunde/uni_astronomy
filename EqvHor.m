function [t_hour,delta_deg,A,h_deg] = EqvHor(JJD, lambda_deg, phi_deg, alpha_h, delta_deg)
  t_rad = GST(JJD - 2400000.5)  + lambda_deg * 2 * pi / 360 - alpha_h * pi / 12;
  
  delta_rad = delta_deg * pi / 180;
  phi_rad = phi_deg * pi / 180;
  
  coshsinA = cos(delta_rad) * sin(t_rad);
  coshcosA = sin(phi_rad) * cos(delta_rad) * cos(t_rad) - cos(phi_rad) * sin(delta_rad);
  sinh = cos(phi_rad) * cos(delta_rad) * cos(t_rad) + sin(phi_rad) * sin(delta_rad);
  
  A_deg = atan2(coshsinA, coshcosA) * 180 / pi;
  A=mod((360 + A_deg),360);
  h_deg = asin(sinh) * 180 / pi;
  
  t_hour = mod((24 + t_rad * 24 / (2 * pi)), 24);
endfunction