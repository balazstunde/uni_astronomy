function [] = PrecMatrixTest()
  epsilon(1:3,1:3)=0.0001;
  
  T1 = 0.182;
  T2 = 0.193;
  
  P1 = PrecMatrix_Ecl(T1, T2);
  P2 = PrecMatrix_Ecl(T2, T1);
  
  P1 + P2 - eye(3) * 2 
  
  if P1 + P2 - eye(3)*2 < epsilon
      fprintf('Correct ecliptical transformation between two epoques\n');
  else
      fprintf('INCORRECT ecliptical transformation between two epoques\n');
  end
  
  P3 = PrecMatrix_Equ(T1, T2);
  P4 = PrecMatrix_Equ(T2, T1);
  
  if P3 + P4 - eye(3)*2 < epsilon
      fprintf('Correct equatorial transformation between two epoques\n');
  else
      fprintf('INCORRECT equatorial transformation between two epoques\n');
  end
endfunction
