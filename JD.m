function [ Juliand ] = JD( ev, honap, nap, ora )
    A = 10000.0*ev + 100.0*honap + nap;
    if(honap <= 2)
       honap = honap + 12;
       ev = ev - 1;
    end
    if(A<=15821004.1)
        B = -2 + fix((ev + 4716)/4) - 1179;
        %fprintf('Julian Calendar');
    else
        B = fix(ev/400) - fix(ev/100) + fix(ev/4);
        %fprintf('Gregorian calendar');
    end
    format long g;
    A = 365.0 * ev - 679004.0;
    MDJ = A + B + fix(30.6001*(honap+1)) + nap + ora/24.0;
    Juliand = MDJ + 2400000.5;
end
