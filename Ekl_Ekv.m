function [RA_hour, Dec_deg] = Ekl_Ekv(lambda_deg, beta_deg, T)
  beta_rad = beta_deg * pi / 180;
  lambda_rad = lambda_deg * pi / 180;
  %beta_rad
  %lambda_rad
  mat(1,1) = cos(beta_rad) * cos(lambda_rad);
  mat(2,1) = cos(beta_rad) * sin(lambda_rad);
  mat(3,1) = sin(beta_rad);
  
  trans_mat = inv(Rot_x(Eps(T) * pi / 180)) * mat;
  %trans_mat
  
  Dec_rad = asin(trans_mat(3,1));
  %Dec_rad
  sin_rec = trans_mat(2,1) / cos(Dec_rad);
  cos_rec = trans_mat(1,1) / cos(Dec_rad);
  Rec_rad = sc_rad(sin_rec, cos_rec);
  %Rec_rad = asin(sin_rec);
  
  RA_hour = (Rec_rad * 24) / (2 * pi);
  Dec_deg = Dec_rad * 180 / pi;
  
  %[dd, dm, ds] = deg_dms(Dec_deg)
endfunction
