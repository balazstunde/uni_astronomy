function[E, iteracio] = Kepler(e, M, epsilon)
  M = mod(M, 2 * pi);
  format long
  tic
  E = M;
  E_1 = M + e * sin(E);
  iteracio = 0;
  while abs(E_1 - E) > epsilon
      iteracio = iteracio + 1;
      E = E_1;
      E_1 = M + e * sin(E);
  endwhile
  E = mod(E, 2 * pi);
  %ido = 0;
  toc
endfunction