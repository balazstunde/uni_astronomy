function [min_month, min_day] = Husvet_elso_datuma(year)
  min_month = 5;
  min_day = 32;
  for i = 1583:year
    [month, day] = Husvet(i);
    if month < min_month
      min_month = month;
      min_day = day;
      disp(i);
    else
      if month == min_month && day < min_day
        min_day = day;
        disp(i);
      endif
    endif
  endfor
endfunction
