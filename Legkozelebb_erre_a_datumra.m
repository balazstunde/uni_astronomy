function [year] = Legkozelebb_erre_a_datumra(_month, _day)
  
  year = 2019;
  while 1
    [month, day] = Husvet(year);
    if month == _month && day == _day
      break;
    endif
    year++;
  endwhile
endfunction
