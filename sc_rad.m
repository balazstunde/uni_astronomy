function r = sc_rad(sr, cr)
  %if(abs(sr ^ 2 - cr ^2)^2 > 0.001)
  %  disp("Helytelen bemenet.");
  %  return;
  %endif
  if(sr >= 0)
    r = acos(cr);
  else
    r = 2*pi - acos(cr);
  endif
endfunction
