function [max_gyakorisag_honap, max_gyakorisag_nap] = Husvet_gyakorisaga_intervallumban(int1, int2)
  gyakorisag = zeros(35, 3);
  month = 3;
  day = 22;
  for i = 1:35
    gyakorisag(i, 1) = month;
    gyakorisag(i, 2) = day;
    if day == 31
      day = 0;
      month ++;
    endif
    day ++;
  endfor
  year = 0;
  for i = int1:int2
    [month, day] = Husvet(i);
    if (month == 4 && day ==11)
      year = i
    endif
    j = 1;
    while j <= 35
      if gyakorisag(j, 1) == month && gyakorisag(j, 2) == day
        break;      
      endif
      j++;
    endwhile
    gyakorisag(j, 3)++;
  endfor
  [~, I] = max(gyakorisag(:,3))
  
  disp("Tablazat:");
  disp(gyakorisag);
  
  max_gyakorisag_honap = gyakorisag(I, 1);
  max_gyakorisag_nap = gyakorisag(I, 2);
endfunction
