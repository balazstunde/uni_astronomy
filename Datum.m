function [year, month, day, hour] = Datum(julian)
  year = -4713;
  
  day = 0;
  hour = 0;
  while 1
    if((JD(1, 1, year, 0) <= julian) && (JD(1,1,year+1,0) > julian))
      break;
    endif
    year ++;
  endwhile
  
  month = 1;
  while month <= 11
    if((JD(1, month, year, 0) <= julian) && (JD(1,month + 1,year,0) > julian))
      break;
    endif
    month++;
  endwhile
  
  day = 1;
  while day <= 30
    if((JD(day, month, year, 0) <= julian) && (JD(day + 1, month, year, 0) > julian))
      break;
    endif
    day++;
  endwhile
  
  hour = 0;
  while 1
    if(JD(day, month, year, hour) >= julian)
      break;
    endif
    hour = hour + 0.1;
  endwhile
  %hour = julian - JD(day, month, year, 0);
  
endfunction
