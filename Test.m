function [] = Test(ev, honap, nap, ora, perc, sec, lambda_fok, lambda_perc, lambda_sec, beta_fok, beta_perc, beta_sec)
  hour = dms_deg(ora, perc, sec);
  JD = JD(ev, honap, nap, hour);
  %disp(JD_T(JD));
  T = JD_T(JD)
  Eps = Eps(T)
  [d, m, s] = deg_dms(Eps)
  lambda_deg = dms_deg(lambda_fok, lambda_perc, lambda_sec)
  beta_deg = dms_deg(beta_fok, beta_perc, beta_sec)
  [RA_hour, Dec_deg] = Ekl_Ekv(lambda_deg, beta_deg, T)
  [dRa, m, s] = deg_dms(RA_hour)
  [dDec, m, s] =deg_dms(Dec_deg)
  %Eps = Eps(T);
  %delta_deg = dms_deg(delta_fok, delta_perc ,delta_sec);
  %T, Eps, alpha_ora, alpha_perc, alpha_sec, delta_fok, delta_perc, delta_sec
endfunction
