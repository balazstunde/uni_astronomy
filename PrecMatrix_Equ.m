function [P] = PrecMatrix_Equ(T1, T2)
  T0 = T1;
  dT = T2 - T1;
  
  zeta = (dms_deg(0,0,2306.2181) + dms_deg(0,0,1.39656) * T0 - dms_deg(0,0,0.000139) * T0^2) * dT;
  zeta = zeta + (dms_deg(0,0,0.30188) - dms_deg(0,0,0.000345) * T0) * dT ^ 2 + dms_deg(0,0,0.017998) * dT ^ 3;
  zeta = zeta * pi / 180
  
  theta = (dms_deg(0,0,2004.3109) - dms_deg(0,0,0.85330) * T0 - dms_deg(0,0,0.000217) * T0^2) * dT;
  theta = theta + (dms_deg(0,0,-0.42665) - dms_deg(0,0,0.000217) * T0) * dT ^ 2 - dms_deg(0,0,0.041833) * dT ^ 3;
  theta = theta * pi / 180
  
  z = zeta + ((dms_deg(0,0,0.79280) + dms_deg(0,0,0.000411) * T0) * dT ^ 2 + dms_deg(0,0,0.000205) * dT ^ 3) * pi / 180
  
  P = Rot_z(-z) * Rot_y(theta) * Rot_z(-zeta);
  
endfunction
