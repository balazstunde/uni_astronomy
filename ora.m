function [ora, perc, secundum] = ora(k, lambda)%helyi ido UT+k, foldrajzni hossz, 3,23.8
  while (1)
    
    d = datetime('now');
    d = d - hours(k);
    
    mjd = JD(d) + 2400000.5;

    s_G = GST(mjd);

    s = mod(s_G + pi*(lambda)/180,2*pi);
    
    h = (s *12)/pi;
    ora = floor(h);
  
    min = (h - ora)*60;
    perc = floor(min);
    
    sec = (min - perc)*60;
    secundum = floor(sec);
  
    disp(d);
    fprintf('%d h %d m %d s \n',ora, perc, secundum);
    
    pause(1);
  endwhile

endfunction
