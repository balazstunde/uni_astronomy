function [] = csillagido(UT_timezone, lambda)
while (1)
  d = localtime(time ());  % a rendszrbol datumot le lehet kerni, ebbol ora perc mp
  d.hour = d.hour - UT_timezone;
  
  hour_float = dms_deg(d.hour, d.min, d.sec);
  jd = JD(d.year + 1900, d.mon + 1, d.mday, hour_float);
  mjd = jd - 2400000.5;
  sid_time_rad = GST(mjd);  %greenwichi csillagido vagy mi
  
  lambda_rad      = deg2rad(lambda);
  sid_time_rad    = mod(sid_time_rad + lambda_rad, 2*pi);

  hour_float  = (sid_time_rad *12)/pi;
  [h,m,s]     = deg_dms(hour_float);

  disp(ctime (time ()));
  fprintf('%d h \t %d m \t %d s \n---------------------\n', h, m, s);
  pause(1);
  
endwhile
endfunction
