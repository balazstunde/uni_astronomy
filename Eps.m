function[eps] = Eps(T)
  eps = 23.43929111 - T * (46.8150 + T * (0.00059 - 0.001813 * T)) / 3600;
endfunction