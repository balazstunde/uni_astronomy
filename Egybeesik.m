function [] = Egybeesik()
  
  for i = 2101:2200
    [mJ, dJ] = Husvet_Julian(i);
    [mG, dG] = Husvet(i);
    dJ += 13;
    if dJ > 31
      dJ -= 31;
      mJ ++;
    endif
    if mJ == mG && dJ == dG
      disp(i)
    endif
  endfor
  
endfunction