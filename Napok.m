function [day_number] = Napok(year, month, day)
  day_number = JD(day, month, year, 0) - JD(1, 1, year, 0) + 1;
  day_name = mod(fix(JD(day, month, year, 0)) ,7)
  if(!Datumellenor(year, month, day))
    disp("error")
    return
  endif
  switch day_name
      case 0 
            disp("Kedd"); 
      case 1 
          disp("Szerda");
      case 2 
          disp("Csutortok");
      case 3
        disp("Pentek");
      case 4
        disp("Szombat");
      case 5
        disp("Vasarnap");
      case 6
        disp("Hetfo");
  endswitch
endfunction
