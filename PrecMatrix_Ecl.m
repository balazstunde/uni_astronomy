function [P] = PrecMatrix_Ecl(T1, T2)
  T0 = T1;
  dT = T2 - T1;
  
  pis = (47.0029 + -0.06603 * T0 + 0.000598 * T0^2) * dT;
  pis = pis + (-0.03302 + 0.000598 * T0) * dT ^ 2 + 0.000060 * dT ^ 3;
  pis = pis * pi / (3600 * 180)
  
  PI = 174.876383889 + dms_deg(0,0,3289.4789) * T0 + dms_deg(0,0,0.60622) * T0^2;
  PI = PI + (dms_deg(0,0,-869.8089) - dms_deg(0,0,0.50491) * T0) * dT + dms_deg(0,0,0.03536) * dT ^ 2;
  PI = PI * pi / 180
  
  
  p = (dms_deg(0,0,5029.0966) + dms_deg(0,0,2.22226) * T0 - dms_deg(0,0,0.000042) * T0 ^ 2) * dT;
  p = p + (dms_deg(0,0,1.11113) - dms_deg(0,0,0.000042) * T0) * dT ^ 2 - dms_deg(0,0,0.000006) * dT ^ 3;
  p = p * pi / 180
  
  lambda = PI + p
  P = Rot_z(-lambda) * Rot_x(pis) * Rot_z(PI);
endfunction
