function[Lmf] = Newton(X, Xf, x)
    Lmf = 0;
    m = length(X);
    n = length(Xf);
    
    if m ~= n
       disp("A dimenziok nem egyeznek meg.")
    end
    tablazat = zeros(m,m);
    
    tablazat(:,1) = Xf;
    %disp(tablazat);
    
    for j=2:m
       for i=1:m-j+1
           tablazat(i,j) = (tablazat(i+1,j-1) - tablazat(i,j-1)) / (X( i + j - 1 ) - X(i));  %X(i) az a legkisebb s soronkent eggyel no a legnagyobb
       end
    end
    
    %disp(tablazat);
   
    Lmf = Lmf + Xf(1);
    szorzat = 1;
    for i = 2:m
        szorzat = szorzat * (x - X(i-1)); 
        Lmf = Lmf + (szorzat * tablazat(1,i));
    end
    
end