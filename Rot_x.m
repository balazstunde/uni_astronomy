function Rx = Rot_x(phi)
  Rx = zeros(3,3);
  Rx(1,1) = 1;
  Rx(1,2) = 0;
  Rx(1,3) = 0;
  
  Rx(2,1) = 0;
  Rx(2,2) = cos(phi);
  Rx(2,3) = sin(phi);
  
  Rx(3,1) = 0;
  Rx(3,2) = -sin(phi);
  Rx(3,3) = cos(phi);
  
endfunction
