function [d,m,s] = Eps1(ev,honap,nap,ora,perc, masodperc)
    ora = dms_deg(ora, perc, masodperc)
    jd = JD(ev,honap,nap,ora)
    T = JD_T(jd)
    %fokban:
    epszilon = 23.4392111 - T * (46.8150 + T * (0.00059 - 0.001813 * T))/3600; 
    [d,m,s] = deg_dms(epszilon);
end