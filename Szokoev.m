function [] = Szokoev(year)
   %if(mod(year, 4) == 0 && (mod(year, 100) == 0 && year >= 1600) || year < 1600)
   %   disp("Szokoev");
   %else 
   %   disp("Nem szokoev.");
   %endif
   
   if(mod(year, 4) == 0)
      if(year >= 1600)
        if(mod(year, 100) == 0)
          if(mod(year/100, 4) == 0)
            disp("Szokoev");   
          else
            disp("Nem szokoev");
          endif
        else
          disp("Szokoev.")
        endif
      else
        disp("Szokoev.");
      endif
    else
      disp("Nem szokoev");
   endif
endfunction
