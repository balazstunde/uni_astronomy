function [greenwichi_csillagido_rad] = GST(MJD)
%  JD = MJD + 2400000.5;
%  [year, month, day, hour] = Datum(JD);
%  JD0 = JD(year, month, day, 0);
  %JD = floor(MJD) + 2400000.5;
  MJD0   = floor(MJD);
  
  JD  = MJD + 2400000.5;
  JD0 = MJD0 + 2400000.5;
    
  UT = 86400 * (JD - JD0);
  
  T0 = (JD0 - 2451545 ) / 36525;
  T = (JD - 2451545) / 36525;
  greenwichi_csillagido_sec = 24110.54841 + 8640184.812866 * T0 + 1.0027379093 * UT + 0.093104 * T^2 - 0.0000062 * T^3;
                          
  %sg_sec = 24110.54841 + 8640184.812866*T0 + 1.0027379093*UT + 0.093104 + T^2 - 0.0000062*T^3;

  greenwichi_csillagido_rad = mod(greenwichi_csillagido_sec*(pi/43200),2*pi);
endfunction
