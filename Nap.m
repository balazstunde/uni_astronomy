function [L_fok, B_fok] = Nap(JD)
  %JD = JD(ev, honap, nap, honap)
  T = JD_T(JD)
  M = 0.993133 + 99.997361 * T;
  M_frac = M - floor(M);
  M_rad = 2 * pi * M_frac;
  
  L = 0.7859453 + M_rad / (2 * pi) + (6893 * sin(M_rad) + 72 * sin(2 * M_rad) + 6191 * T) / 1296000;
  L_frac = L - floor(L);
  L_rad = 2 * pi * L_frac;
  L_fok = L_rad * 180 / pi;
  B_fok = 0;
endfunction
