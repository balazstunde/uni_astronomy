function[a, e, i, nagy_omega, kicsi_omega, M] = Palyaelemek(n, T)
format long
switch (n)
  case 1
    a = 0.38709927 + 0.00000037 * T;
    e = 0.20563593 + 0.00001906 * T;
    i = 7.00497902 - 0.00594749 * T;
    L = 252.25032350 + 149472.67411175 * T;
    omega_tilda = 77.45779628 + 0.16047689 * T;
    nagy_omega = 48.33076593 - 0.12534081 * T;
    kicsi_omega = omega_tilda - nagy_omega;
    M = mod(L - omega_tilda, 360);
    i = mod(i, 180);
    nagy_omega = mod(nagy_omega, 360);
    kicsi_omega = mod(kicsi_omega, 360);
  case 2
    a = 0.72333566 + 0.00000390 * T;
    e = 0.00677672 - 0.00004107 * T;
    i = 3.39467605 - 0.00078890 * T;
    L = 181.97909950 + 58517.81538729 * T;
    omega_tilda = 131.60246718 + 0.00268329 * T;
    nagy_omega = 76.67984255 - 0.27769418 * T;
    kicsi_omega = omega_tilda - nagy_omega;
    M = mod(L - omega_tilda, 360);
    i = mod(i, 180);
    nagy_omega = mod(nagy_omega, 360);
    kicsi_omega = mod(kicsi_omega, 360);
  case 3
    a = 1.00000261 + 0.00000562 * T;
    e = 0.01671123 - 0.00004392 * T;
    i = -0.00001531 - 0.01294668 * T;
    L = 100.46457166 + 35999.37244981 * T;
    omega_tilda = 102.93768193 + 0.32327364 * T;
    nagy_omega = 0.0;
    kicsi_omega = omega_tilda - nagy_omega;
    M = mod(L - omega_tilda, 360);
    i = mod(i, 180);
    nagy_omega = mod(nagy_omega, 360);
    kicsi_omega = mod(kicsi_omega, 360);
  case 4
    a = 1.52371034 + 0.00001847 * T;
    e = 0.09339410 + 0.00007882 * T;
    i = 1.84969142 - 0.00813131 * T;
    L = -4.55343205 + 19140.30268499 * T;
    omega_tilda = -23.94362959 + 0.44441088 * T;
    nagy_omega = 49.55953891 - 0.29257343 * T;
    kicsi_omega = omega_tilda - nagy_omega;
    M = mod(L - omega_tilda, 360);
    i = mod(i, 180);
    nagy_omega = mod(nagy_omega, 360);
    kicsi_omega = mod(kicsi_omega, 360);
  case 5
    a = 5.20288700 - 0.00011607 * T;
    e = 0.04838624 - 0.00013253 * T;
    i = 1.30439695 - 0.00183714 * T;
    L = 34.39644051 + 3034.74612775 * T;
    omega_tilda = 14.72847983 + 0.21252668 * T;
    nagy_omega = 100.47390909 + 0.20469106 * T;
    kicsi_omega = omega_tilda - nagy_omega;
    M = mod(L - omega_tilda, 360);
    i = mod(i, 180);
    nagy_omega = mod(nagy_omega, 360);
    kicsi_omega = mod(kicsi_omega, 360);
  case 6
    a = 9.53667594 - 0.00125060 * T;
    e = 0.05386179 -0.00050991 * T;
    i = 2.48599187 + 0.00193609 * T;
    L = 49.95424423 + 1222.49362201 * T;
    omega_tilda = 92.59887831 + 0.41897216 * T;
    nagy_omega = 113.66242448 + 0.28867794 * T;
    kicsi_omega = omega_tilda - nagy_omega;
    M = mod(L - omega_tilda, 360);
    i = mod(i, 180);
    nagy_omega = mod(nagy_omega, 360);
    kicsi_omega = mod(kicsi_omega, 360);
  case 7
    a = 19.18916464 -0.00196176 * T;
    e = 0.04725744 -0.00004397 * T;
    i = 0.77263783 -0.00242939 * T;
    L = 313.23810451 + 428.48202785  * T;
    omega_tilda = 170.95427630 + 0.40805281 * T;
    nagy_omega = 74.01692503 + 0.04240589 * T;
    kicsi_omega = omega_tilda - nagy_omega;
    M = mod(L - omega_tilda, 360);
    i = mod(i, 180);
    nagy_omega = mod(nagy_omega, 360);
    kicsi_omega = mod(kicsi_omega, 360);
  case 8
    a = 30.06992276 + 0.00026291 * T;
    e = 0.00859048 + 0.00005105 * T;
    i = 1.77004347 + 0.00035372 * T;
    L = -55.12002969  + 218.45945325 * T;
    omega_tilda = 44.96476227 -0.32241464 * T;
    nagy_omega = 131.78422574 -0.00508664 * T;
    kicsi_omega = omega_tilda - nagy_omega;
    M = mod(L - omega_tilda, 360);
    i = mod(i, 180);
    nagy_omega = mod(nagy_omega, 360);
    kicsi_omega = mod(kicsi_omega, 360);
  case 9
    a = 39.48211675 -0.00031596 * T;
    e = 0.24882730 + 0.00005170 * T;
    i = 17.14001206 + 0.00004818 * T;
    L = 238.92903833 + 145.20780515 * T;
    omega_tilda = 224.06891629 -0.04062942 * T;
    nagy_omega = 110.30393684 -0.01183482 * T;
    kicsi_omega = omega_tilda - nagy_omega;
    M = mod(L - omega_tilda, 360);
    i = mod(i, 180);
    nagy_omega = mod(nagy_omega, 360);
    kicsi_omega = mod(kicsi_omega, 360);
  otherwise
    a = 0;
    e = 0;
    i = 0; 
    M = 0;
    kicsi_omega = 0;
    nagy_omega = 0;
endswitch

endfunction