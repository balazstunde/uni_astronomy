function [max_month, max_day] = Husvet_utolso_datuma(year)
  max_month = 2;
  max_day = 0;
  for i = 1583:year
    [month, day] = Husvet(i);
    if month > max_month
      max_month = month;
      max_day = day;
      disp(i);
    else
      if month == max_month && day > max_day
        max_day = day;
        disp(i);
      endif
    endif
  endfor
endfunction
