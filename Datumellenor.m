function [igaz] = Datumellenor(year, month, day)
  julian = JD(day, month, year, 0)
  [_year, _month, _day, _hour] = Datum(julian)
  if(_year == year && _month ==month && _day == day)
    disp("Helyes datum.");
    igaz = true;
  else
    disp("Helytelen datum.");
    igaz = false;
  endif
endfunction
