function [r_ekl] = Bolygo_ekl(n, T)
  format long
  [a, e, i, nagy_omega, kicsi_omega, M] = Palyaelemek(n, T);
  M = M  * pi / 180;
  [E, iteracio] = Kepler(e, M, eps);
  x_ = a * (cos(E) - e);
  y_ = a * sqrt(1 - e^2) * sin(E);
  z_ = 0;
  r = [x_; y_; z_];
  
  nagy_omega = nagy_omega * pi / 180;
  kicsi_omega = kicsi_omega * pi / 180;
  i = i * pi / 180;
  r_ekl = Rot_z(-nagy_omega) * Rot_x(-i) * Rot_z(-kicsi_omega) * r;
  
endfunction
