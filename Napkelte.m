function [kelte_ora, kelte_perc, nyugta_ora, nyugta_perc] = Napkelte(ev, honap, nap, lambda, phi_deg, k)
  ora = 3;
  perc = 0;
  h_deg = -360;
  h0 = dms_deg(0,-50,0);
  kelte_perc = 0;
  nyugta_ora = 0;
  nyugta_perc = 0;
    
  while h_deg < h0
    hour = dms_deg(ora, perc, 0)
    jd = JD(ev, honap, nap, hour)
    [lambda_, beta] = Nap(jd)

    [RA, Dec] = Ekl_Ekv(lambda_, beta, JD_T(jd));
    [t_hour, delta_deg, A, h_deg] = EqvHor(jd, lambda, phi_deg, RA, Dec);
    ora = ora + 1;
  endwhile
  kelte_ora = ora - 1 + k;

  h_deg = 360;
  while h_deg > h0
    hour = dms_deg(ora, perc, 0);
    jd = JD(ev, honap, nap, hour);
    [lambda_, beta] = Nap(jd);
    
    [RA, Dec] = Ekl_Ekv(lambda_, beta, JD_T(jd));
    [t_hour, delta_deg, A, h_deg] = EqvHor(jd, lambda, phi_deg, RA, Dec)
    ora = ora + 1;
  endwhile
  nyugta_ora = ora - 1 + k;
  
  h_deg = -360;
  while h_deg < h0
    hour = dms_deg(kelte_ora, perc, 0)
    jd = JD(ev, honap, nap, hour)
    [lambda_, beta] = Nap(jd)
    
    [RA, Dec] = Ekl_Ekv(lambda_, beta, JD_T(jd))
    [t_hour, delta_deg, A, h_deg] = EqvHor(jd, lambda, phi_deg, RA, Dec)
    perc = perc + 1;
  endwhile
  kelte_perc = perc;
  
  perc = 0;
  h_deg = 360;
  while h_deg > h0
    hour = dms_deg(nyugta_ora, perc, 0)
    jd = JD(ev, honap, nap, hour)
    [lambda_, beta] = Nap(jd)
    
    [RA, Dec] = Ekl_Ekv(lambda_, beta, JD_T(jd))
    [t_hour, delta_deg, A, h_deg] = EqvHor(jd, lambda, phi_deg, RA, Dec)
    perc = perc + 1;
  endwhile
  nyugta_perc = perc;

%nap ekliptikai koordinataja -> Rec, Dec -> A, h (ejjel a nap horizont alatt,
% majd a horizont folott, valahol delel a legmagasabban,
% s aztan vissza a horizontra s lebukik,
% -50 perc a horizont folotti magassag, az erdekel engem, mert ekkor kel es nyugszik
% orankent nap koordinatak, mikor van meg alatta s mikor van meg folotte, ha igy van, 
% akkor azt hasznalom napkeltenek, majd ugyanigy a napnyugtat, majd veszem a perceket)
endfunction