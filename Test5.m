function [] = Test5(ev1, honap1, nap1, ora1, min1, sec1, ev2, honap2, nap2, ora2, min2, sec2)
  hour1 = dms_deg(ora1, min1, sec1);
  JD1 = JD(ev1, honap1, nap1, hour1);
  %disp(JD_T(JD));
  T1 = JD_T(JD1)
  
  hour2 = dms_deg(ora2, min2, sec2);
  JD2 = JD(ev2, honap2, nap2, hour2);
  %disp(JD_T(JD));
  T2 = JD_T(JD2)
  
  ecl = PrecMatrix_Ecl(T1, T2)
  equ = PrecMatrix_Equ(T1, T2)
endfunction
