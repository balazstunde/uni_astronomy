function [lambda_deg, beta_deg] = Ekv_Ekl(RA_hour, Dec_deg, T)
  mat = zeros(3,1);
  Dec_rad = Dec_deg * (pi / 180);
  RA_rad = (2 * pi * RA_hour) / 24;
  mat(1,1) = cos(Dec_rad) * cos(RA_rad);
  mat(2,1) = cos(Dec_rad) * sin(RA_rad);
  mat(3,1) = sin(Dec_rad);
  
  trans_mat = Rot_x(Eps(T) * pi / 180) * mat;
  
  beta_rad = asin(trans_mat(3,1));
  cos_lambda = trans_mat(1,1) / cos(beta_rad);
  sin_lambda = trans_mat(2,1) / cos(beta_rad);
  
  beta_deg = beta_rad * 180 / pi;
  lambda_rad  = sc_rad(sin_lambda, cos_lambda);
  %lambda_rad = asin(sin_lambda);
  lambda_deg = lambda_rad * 180 / pi;
  
  [ld,lm,ls] = deg_dms(lambda_deg)
  [bd,bm,bs] = deg_dms(beta_deg)
  
endfunction
