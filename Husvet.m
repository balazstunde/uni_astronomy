function [month, day] = Husvet(year)
  if year < 1538
    disp("Year < 1538.")
    return;
  endif
  
  a = mod(year, 19);
  b = floor(year / 100);
  c = mod(year, 100);
  d = floor(b / 4);
  e = mod(b, 4);
  f = floor((b + 8) / 25);
  g = floor((b - f + 1) / 3);
  h = mod(19 * a + b - d -g + 15, 30);
  i = floor(c / 4);
  k = mod(c, 4);
  l = mod(32 + 2 * e + 2 * i - h - k, 7);
  m = floor((a + 11 * h + 22 * l) / 451);
  n = floor((h + l - 7 * m + 114) / 31);
  p = mod(h + l - 7 * m + 114 , 31);
  month = n;
  day = p + 1;
endfunction
