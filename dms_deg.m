function deg = dms_deg(d, m ,s)
  neg = 1;
  if(d < 0)
    d = -d;
    neg = -neg;
  elseif(m < 0)
    m = -m;
    neg = -neg;
  elseif(s < 0)
    s = -s;
    neg = -neg;
  endif
  deg = d + m / 60 + s / 3600;
  deg = neg * deg;
endfunction
