function [tablazat] = Husvet_Tablazat_2019_2100()
  tablazat = zeros(81, 3)
  for i = 1:81
    tablazat(i, 1) = 2019 + i - 1;
    [month, day] = Husvet(2019 + i - 1);
    tablazat(i, 2) = month;
    tablazat(i, 3) = day;
  endfor
endfunction
