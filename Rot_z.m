function Rz = Rot_z(phi)
  Rz = zeros(3,3);
  Rz(1,1) = cos(phi);
  Rz(1,2) = sin(phi);
  Rz(1,3) = 0;
  
  Rz(2,1) = -sin(phi);
  Rz(2,2) = cos(phi);
  Rz(2,3) = 0;
  
  Rz(3,1) = 0;
  Rz(3,2) = 0;
  Rz(3,3) = 1;
  
endfunction
