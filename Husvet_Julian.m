function [month, day] = Husvet_Julian (year)
  a = mod(year, 4);
  b = mod(year, 7);
  c = mod(year, 19);
  d = mod(19 * c + 15, 30);
  e = mod(2 * a + 4 * b - d + 34, 7);
  f = floor((d + e + 114) / 31);
  g = mod(d + e + 114, 31);
  month = f;
  day = g + 1;
endfunction
