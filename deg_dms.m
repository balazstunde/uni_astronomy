function [d, m ,s] = deg_dms(deg)
  if (deg < 0)
    deg = -deg;
    d = floor(deg);
    %disp(deg);
    deg = (deg - floor(deg)) * 60;
    %disp(deg);
    m = floor(deg);
    deg = (deg - floor(deg)) * 60;
    %disp(deg);
    s = floor(deg);
    if(d != 0)
      d = -d;
    elseif (m != 0)
      m = -m;
    else
      s = -s;
    endif
  else
    d = floor(deg);
    %disp(deg);
    deg = (deg - floor(deg)) * 60;
    %disp(deg);
    m = floor(deg);
    deg = (deg - floor(deg)) * 60;
    %disp(deg);
    s = floor(deg);
  endif
endfunction
