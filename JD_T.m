function [T] = JD_T(julian)
  start = JD(2000, 1, 1, 12);
  T = (julian - 2451545) / 36525;
endfunction
