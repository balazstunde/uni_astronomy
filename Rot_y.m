function Ry = Rot_y(phi)
  Ry = zeros(3,3);
  Ry(1,1) = cos(phi);
  Ry(1,2) = 0;
  Ry(1,3) = -sin(phi);
  
  Ry(2,1) = 0;
  Ry(2,2) = 1;
  Ry(2,3) = 0;
  
  Ry(3,1) = sin(phi);
  Ry(3,2) = 0;
  Ry(3,3) = cos(phi);
  
endfunction
